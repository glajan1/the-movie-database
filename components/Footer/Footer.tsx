import S from "./styles.module.scss"

const Footer = () => {
  return <footer className={S.footer}>The Movie Database</footer>
}

export default Footer
