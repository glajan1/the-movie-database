export const img_url = "https://image.tmdb.org/t/p/w1280"
export const api_url = `https://api.themoviedb.org/3`

const api_key = "22700e74472b3b1549eb925399f3cf4c"

export const appendApiKey = (url:string) => {
    const newURL = new URL(url)
    const params = new URLSearchParams(newURL.search)
    params.append('api_key', api_key)
    newURL.search = params.toString()
    return newURL
}
