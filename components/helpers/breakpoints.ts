export const breakpoints: { [key: string]: number } = {
    /** min-width: 576px */
    mobileAndUp: 576,
  
    /** min-width: 768px */
    tabletAndUp: 768,
  
    /** min-width: 1024px */
    desktopAndUp: 1024,
  
    /** min-width: 1280px */
    largeDesktopAndUp: 1280,
  
    /** min-width: 1520px */
    maxWidthAndUp: 1520,
  
    /** min-width: 1776px */
    wrapperMaxWidth: 1776,
  }
  
  export const media = {
    /** min-width: 576px */
    mobileAndUp: `(min-width: ${breakpoints.mobileAndUp}px)`,
  
    /** min-width: 768px */
    tabletAndUp: `(min-width: ${breakpoints.tabletAndUp}px)`,
  
    /** min-width: 1024px */
    desktopAndUp: `(min-width: ${breakpoints.desktopAndUp}px)`,
  
    /** min-width: 1280px */
    largeDesktopAndUp: `(min-width: ${breakpoints.largeDesktopAndUp}px)`,
  
    /** min-width: 1520px */
    maxWidthAndUp: `(min-width: ${breakpoints.maxWidthAndUp}px)`,
  
    /** min-width: 1776px */
    wrapperMaxWidth: `(min-width: ${breakpoints.wrapperMaxWidth}px)`,
  }