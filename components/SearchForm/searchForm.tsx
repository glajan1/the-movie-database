"use client"

import { useRouter } from "next/navigation"
import {
  ChangeEvent,
  FormEvent,
  FormEventHandler,
  useEffect,
  useState,
} from "react"
import S from "./styles.module.scss"
import { Props } from "./types"

export const SearchForm = (p: Props) => {
  const router = useRouter()
  const [searchTerm, setSearchTerm] = useState("")

  const handleSubmit: FormEventHandler = (e: FormEvent) => {
    e.preventDefault()
    if (!searchTerm) return
    router.push("/search?query=" + searchTerm)
  }

  const handleChangeSearchTerm = (e: ChangeEvent<HTMLInputElement>) => {
    const searchTerm = e.target.value
    setSearchTerm(searchTerm)
  }

  useEffect(() => {
    setSearchTerm(p.searchTerm)
  }, [p.searchTerm])

  return (
    <form className={S.form} onSubmit={handleSubmit}>
      <input
        className={S.textInput}
        type="text"
        id="searchTerm"
        name="searchTerm"
        placeholder="Search for a movie..."
        value={searchTerm}
        onChange={handleChangeSearchTerm}
        autoFocus
      />
      <input className={S.button} type="submit" value="Search" />
    </form>
  )
}
