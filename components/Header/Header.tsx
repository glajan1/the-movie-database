import Link from "next/link"
import S from "./styles.module.scss"

const Header = () => {
  return (
    <header className={S.header}>
      <div className={S.logo}></div>
      <h1>
        <Link href="/">The Movie Database</Link>
      </h1>
      <Link className={S.searchLink} href="/search">
        Search
      </Link>
    </header>
  )
}

export default Header
