import { api_url, appendApiKey, img_url } from "@/components/helpers/urls"
import S from "../page.module.scss"
import * as T from "../types"

async function fetchMovie(movieId: string) {
  const url = appendApiKey(`${api_url}/movie/${movieId}`)
  const res = await fetch(url)

  if (!res) {
    throw new Error("Failed to fetch movie data.")
  }

  return res.json()
}

export default async function Movie({ params }: T.MoviePage) {
  const res: T.Movie = await fetchMovie(params.id)

  return (
    <main
      className={S.main}
      style={{ backgroundImage: `url(${img_url}/${res.backdrop_path})` }}
    >
      <div className={S.movie}>
        <div className={S.poster}>
          <img className={S.posterImg} src={`${img_url}/${res.poster_path}`} />
        </div>
        <div className={S.movieInfo}>
          <h1>{res.original_title}</h1>
          <h2>{res.tagline}</h2>
          <div className={S.released}>Released {res.release_date}</div>
          <div>{res.overview}</div>
        </div>
      </div>
    </main>
  )
}
