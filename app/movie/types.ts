export interface Movie {
  adult: boolean
  backdrop_path: string
  budget: number
  genres: [{ id: number; name: string }]
  homepage: string
  id: number
  original_language: string
  original_title: string
  overview: string
  popularity: number
  poster_path: string
  release_date: string
  revenue: number
  runtime: number
  status: string
  tagline: string
  title: string
  video: boolean
  vote_average: number
  vote_count: number
}

export interface MoviePage {
  params: { id: string }
}
