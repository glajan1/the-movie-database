import { SearchForm } from "@/components/SearchForm/searchForm"
import Link from "next/link"
import S from "./page.module.scss"
import backgroundImage from "../../public/background2.jpg"
import * as T from "./types"
import { api_url, appendApiKey, img_url } from "@/components/helpers/urls"

async function fetchSearchResults(query: string) {
  const url = appendApiKey(`${api_url}/search/movie?query=${query}`)

  const res = await fetch(url, { cache: "no-store" })

  if (!res) {
    throw new Error("Failed to get search response.")
  }

  return res.json()
}

export default async function Search({ params, searchParams }: T.SearchPage) {
  const query = (
    Array.isArray(searchParams?.query)
      ? searchParams?.query[0]
      : searchParams?.query || ""
  ) as string

  const searchResults = await fetchSearchResults(query)

  return (
    <main
      className={S.main}
      style={{ backgroundImage: `url('${backgroundImage.src}')` }}
    >
      <SearchForm searchTerm={query} />

      <div className={S.searchResults}>
        {query && searchResults?.total_results === 0 ? (
          <div className={S.notFoundMessage}>No movies found for: {query}</div>
        ) : (
          searchResults?.results?.map((result: T.SearchResultsItem) => {
            return (
              <Link
                href={`/movie/${result.id}`}
                className={S.searchResultsItem}
                key={result.id}
              >
                <div className={S.moviePoster}>
                  <img
                    src={`${img_url}/${result.poster_path}`}
                    alt="Movie poster"
                  />
                </div>
                <div className={S.movieInfo}>
                  <h4 className={S.movieTitle}>{result.original_title}</h4>
                  <div className={S.movieOverview}>{result.overview}</div>
                  <div className={S.fadeOut}></div>
                </div>
              </Link>
            )
          })
        )}
      </div>
    </main>
  )
}
