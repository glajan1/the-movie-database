import S from "./page.module.scss"
import { SearchForm } from "../components/SearchForm/searchForm"
import backgroundImage from "../public/background.jpg"

export default function Home() {
  return (
    <main
      className={S.main}
      style={{ backgroundImage: `url('${backgroundImage.src}')` }}
    >
      <SearchForm searchTerm="" />
    </main>
  )
}
